<?php
/**
 * Lofmp Auction ControllerActionPredispatchCheckoutCartAdd Observer Model.
 * @category  Lofmp
 * @package   Lofmp_Auction
 * @author    Lofmp
 * @copyright Copyright (c) 2010-2017 Lofmp Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Lofmp\Auction\Observer;

use Magento\Framework\Event\ObserverInterface;

class ControllerActionPredispatchCheckoutCartAdd implements ObserverInterface
{
    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $_messageManager;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var \Lofmp\Auction\Model\ProductFactory
     */
    protected $_auctionProductFactory;

    /**
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Checkout\Model\Session             $checkoutSession
     * @param \Lofmp\Auction\Model\ProductFactory        $auctionProductFactory
     */
    public function __construct(
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Lofmp\Auction\Model\ProductFactory $auctionProductFactory
    ) {
    
        $this->_messageManager = $messageManager;
        $this->_checkoutSession = $checkoutSession;
        $this->_auctionProductFactory = $auctionProductFactory;
    }

    /**
     * add to cart event handler.
     *
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $controller = $observer->getControllerAction();
        $cartItems = $this->_checkoutSession->getQuote()->getAllItems();
        foreach ($cartItems as $item) {
            $auctionProduct = $this->_auctionProductFactory->create()->getCollection()
                                            ->addFieldToFilter('product_id', ['eq' => $item->getProductId()])
                                            ->addFieldToFilter('status', ['eq' => 1])->getFirstItem();
            if ($auctionProduct->getEntityId() && count($cartItems) > 1) {
                $msg = 'You can not add another product with auction Product.';
                $this->_messageManager->addError(__($msg));
                $observer->getRequest()->setParam('product', false);
                return $this;
            }
        }
        return $this;
    }
}
