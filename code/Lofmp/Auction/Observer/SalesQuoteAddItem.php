<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lofmp_Auction
 * @copyright  Copyright (c) 2017 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lofmp\Auction\Observer;

use Magento\Framework\Event\ObserverInterface;

class SalesQuoteAddItem implements ObserverInterface
{

    /**
     * @var \Lofmp\Auction\Model\ProductFactory
     */

    protected $_messageManager;

    /**
     * @var \Lofmp\Auction\Model\ProductFactory
     */

    protected $_customerSession;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var \Lofmp\Auction\Model\ProductFactory
     */
    protected $_auctionProductFactory;

    /**
     * @var \Lofmp\Auction\Helper\Data
     */

    protected $_dataHelper;

    /**
     * @var \Lofmp\Auction\Model\WinnerDataFactory
     */

    protected $_winnerData;

    /**
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Customer\Model\Session             $customerSession
     * @param \Magento\Framework\App\RequestInterface     $request
     * @param \Lofmp\Auction\Model\ProductFactory        $auctionProductFactory
     * @param \Lofmp\Auction\Helper\Data                 $dataHelper
     * @param \Lofmp\Auction\Model\WinnerDataFactory     $winnerData
     */
    public function __construct(
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\App\RequestInterface $request,
        \Lofmp\Auction\Model\ProductFactory $auctionProductFactory,
        \Lofmp\Auction\Helper\Data $dataHelper,
        \Lofmp\Auction\Model\WinnerDataFactory $winnerData
    ) {
    
        $this->_messageManager = $messageManager;
        $this->_customerSession = $customerSession;
        $this->_request = $request;
        $this->_auctionProductFactory = $auctionProductFactory;
        $this->_dataHelper = $dataHelper;
        $this->_winnerData = $winnerData;
    }

    /**
     * Sales quote add item event handler.
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {

        if ($this->_customerSession->isLoggedIn()) {
            //$price = '';
            $customerId = $this->_customerSession->getCustomerId();
            $item = $observer->getQuoteItem();
            $product = $item->getProduct();
            $productId = $product->getId();
            if ($product->getTypeId()=='configurable') {
                $superAttribute = $this->_request->getParam('super_attribute');
                if ($superAttribute) {
                    if (count($superAttribute)>0) {
                        //get product id according to attribute values
                        $pro = $product->getTypeInstance(true)->getProductByAttributes($superAttribute, $product);
                        $productId = $pro->getId();
                    }
                }
            }
            $biddingProductCollection = $this->_winnerData->create()->getCollection()
                                                    ->addFieldToFilter('product_id', ['eq' => $productId])
                                                    ->addFieldToFilter('status', ['eq' => 1])
                                                    ->addFieldToFilter('customer_id', ['eq' => $customerId])
                                                    ->addFieldToFilter('complete', ['eq' => 0])
                                                    ->setOrder('auction_id')->getFirstItem();

            if ($biddingProductCollection->getEntityId()) {
                $auctionId = $biddingProductCollection->getAuctionId();
                $auctionPro = $this->_auctionProductFactory->create()->load($auctionId);
                if ($auctionPro->getEntityId() && $auctionPro->getAuctionStatus() == 0) {
                    $bidWinPrice = $biddingProductCollection->getWinAmount();
                   
                    $item->setOriginalCustomPrice($bidWinPrice);
                    $item->setCustomPrice($bidWinPrice);
                    $item->getProduct()->setIsSuperMode(true);
                }
            }
        }
        return $this;
    }
}
