<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lofmp_Auction
 * @copyright  Copyright (c) 2017 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lofmp\Auction\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Customer\Model\Customer;
use Magento\Catalog\Model\Product;

/**
 * Reports Event observer model.
 */
class CatalogControllerProductView implements ObserverInterface
{
    /**
     * @var \Magento\Framework\Stdlib\DateTime
     */
    protected $_dateTime;

    /**
     * @var Configurable
     */
    protected $_configurableProTypeModel;

    /**
     * @var \Magento\Customer\Model\Customer
     */
    protected $_customer;

    /**
     * @var Product
     */
    protected $_product;

    /**
     * @var \Lofmp\Auction\Helper\Data
     */
    protected $_helperData;

    /**
     * @var \Lofmp\Auction\Helper\Email
     */
    protected $_helperEmail;

    /**
     * @var \Lofmp\Auction\Model\ProductFactory
     */
    protected $_auctionProductFactory;

    /**
     * @var \Lofmp\Auction\Model\Amount
     */
    protected $_auctionAmount;

    /**
     * @var \Lofmp\Auction\Model\AutoAuction
     */
    protected $_autoAuction;

    /**
     * @var \Lofmp\Auction\Model\WinnerData
     */
    protected $_winnerData;

    /**
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param Configurable                                $configProTypeModel
     * @param Customer                                    $customer
     * @param Product                                     $product
     * @param \Lofmp\Auction\Helper\Data                 $helperData
     * @param \Lofmp\Auction\Helper\Email                $helperEmail
     * @param \Lofmp\Auction\Model\ProductFactory        $auctionProductFactory
     * @param \Lofmp\Auction\Model\AmountFactory         $auctionAmount
     * @param \Lofmp\Auction\Model\AutoAuctionFactory    $autoAuction
     * @param \Lofmp\Auction\Model\WinnerDataFactory     $winnerData
     */

    public function __construct(
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        Configurable $configProTypeModel,
        Customer $customer,
        Product $product,
        \Lofmp\Auction\Helper\Data $helperData,
        \Lofmp\Auction\Helper\Email $helperEmail,
        \Lofmp\Auction\Model\ProductFactory $auctionProductFactory,
        \Lofmp\Auction\Model\AmountFactory $auctionAmount,
        \Lofmp\Auction\Model\AutoAuctionFactory $autoAuction,
        \Lofmp\Auction\Model\WinnerDataFactory $winnerData
    ) {
    
        $this->_dateTime = $dateTime;
        $this->_configurableProTypeModel = $configProTypeModel;
        $this->_customer = $customer;
        $this->_product = $product;
        $this->_helperData = $helperData;
        $this->_helperEmail = $helperEmail;
        $this->_auctionProductFactory = $auctionProductFactory;
        $this->_auctionAmount = $auctionAmount;
        $this->_autoAuction = $autoAuction;
        $this->_winnerData = $winnerData;
    }

    /**
     * View Catalog Product View observer.
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
       

        $auctionConfig = $this->_helperData->getAuctionConfiguration();
        $product = $observer->getEvent()->getProduct();
        $productId = $product->getId();
        if ($product && $auctionConfig['enable']) {
            $productId = $product->getId();
            if ($product->getTypeId() == 'configurable') {
                $childPro = $this->_configurableProTypeModel->getChildrenIds($productId);
                $childProIds = isset($childPro[0]) ? $childPro[0]:[0];
                $auctionActPro = $this->_auctionProductFactory->create()->getCollection()
                                            ->addFieldToFilter('product_id', ['in' => $childProIds])
                                            ->addFieldToFilter('auction_status', 1)->addFieldToFilter('status', 1);
                if (count($auctionActPro)) {
                    foreach ($auctionActPro as $value) {
                        $this->biddingOperation($value->getProductId());
                    }
                }
            } else {
                $this->biddingOperation($productId);
            }
        }
        return $this;
    }

    /**
     * biddingOperation
     * @param int $productId on which auction apply
     * @return void
     */
    public function biddingOperation($productId)
    {
        $winnerCustomerId = '';
        $bidDay = '';
        $stop = '';
        $addToWin = 0;
        $auctionConfig = $this->_helperData->getAuctionConfiguration();
        $auctionActPro = $this->_auctionProductFactory->create()->getCollection()
                                ->addFieldToFilter('product_id', ['eq' => $productId])
                                ->addFieldToFilter('auction_status', 1)
                                ->addFieldToFilter('status', 0)->getFirstItem();                                           
        if ($auctionActPro->getEntityId()) {
            $resPrice = 0;
            $resPriceConfig = $auctionConfig['reserve_enable'] ? $auctionConfig['reserve_price'] : '';
            
            $bidDay = $auctionActPro->getDays();
            $bidId = $auctionActPro->getEntityId();
            
            $incPrice = $auctionActPro->getIncrementPrice();
            $resPrice = $auctionActPro->getReservePrice();
           
            $resPrice = $resPrice == 0 ? $resPriceConfig:$resPrice;
            $datestop = strtotime($auctionActPro->getStopAuctionTime());
            $datestart = strtotime($auctionActPro->getStartAuctionTime());
            
            if ($datestop >= $datestart) {
              
                $winDataTemp['auction_id'] = $auctionActPro->getEntityId();
                $today = $this->_dateTime->date('Y-m-d H:i:s');
                $current = strtotime($today);
                $difference = $datestop - $current;
              
                if ($difference <= 0) {
                    $bidArray = [];

                    $bidAmountList = $this->_auctionAmount->create()->getCollection()
                                            ->addFieldToFilter('product_id', ['eq' => $productId])
                                            ->addFieldToFilter('auction_id', ['eq' => $bidId])
                                            ->addFieldToFilter('status', ['eq' => 1])
                                            ->setOrder('auction_amount', 'ASC');
                    foreach ($bidAmountList as $bidAmt) {
                        $bidArray[$bidAmt->getCustomerId()] = $bidAmt->getAuctionAmount();
                    }
                    
                    //arsort($bidArray);
                    $autoBidArray = [];
                    /***/
                    if (count($bidArray)) {
                        $customerIds = array_keys($bidArray, max($bidArray));
                        $winDataTemp['customer_id'] = $customerIds[0];
                        $winDataTemp['amount'] = max($bidArray);
                        $winDataTemp['type'] = 'normal';
                    }
                    $autoBidFlag = false;
                    if ($auctionConfig['auto_enable'] && $auctionActPro->getAutoAuctionOpt()) {
                        $autoBidList = $this->_autoAuction->create()->getCollection()
                                                ->addFieldToFilter('auction_id', ['eq' => $bidId])
                                                ->addFieldToFilter('status', ['eq' => 1]);
                        if (count($bidArray)) {
                            $autoBidList->addFieldToFilter('amount', ['gteq'=> max($bidArray)]);
                            $winDataTemp['amount'] = max($bidArray);
                        } else {
                            $resPrice = $auctionActPro->getReservePrice();
                            $starPrice = $auctionActPro->getStartingPrice();
                            $winDataTemp['amount'] = $resPrice ? $resPrice : $starPrice;
                        }

                        if (count($autoBidList)) {
                            foreach ($autoBidList as $autoBid) {
                                $custId = $autoBid->getCustomerId();
                                $autoBidArray[$custId] = $autoBid->getAmount();
                            }
                            $customerIds = array_keys($autoBidArray, max($autoBidArray));
                            $winDataTemp['customer_id'] = $customerIds[0];
                            $winDataTemp['type'] = 'auto';
                        }

                        if ($auctionConfig['increment_auc_enable'] && $auctionActPro->getIncrementOpt()
                            && isset($winDataTemp['customer_id']) && $winDataTemp['type'] = 'auto') {
                            $incVal = $this->_helperData->getIncrementPriceAsRange($winDataTemp['amount']);
                            $incMinPrice = $incVal? $incVal : 0;
                            $winDataTemp['amount'] = $winDataTemp['amount']+$incMinPrice;
                        }
                    }
          
                    if (isset($winDataTemp['customer_id']) && $resPrice <= $winDataTemp['amount']) {
                        if ($winDataTemp['type']=='auto') {
                            $autoBiddList = $this->_autoAuction->create()->getCollection()
                                                    ->addFieldToFilter('auction_id', ['eq' => $bidId])
                                                    ->addFieldToFilter('status', 1);

                            foreach ($autoBiddList as $autoBid) {
                                if ($autoBid->getCustomerId() == $winDataTemp['customer_id']) {
                                    $autoBid->setFlag(1);
                                    $autoBid->setWinningPrice($winDataTemp['amount']);
                                    /** send notification mail to winner */
                                    if ($auctionConfig['enable_winner_notify_email']) {
                                        $this->_helperEmail->sendWinnerMail(
                                            $winDataTemp['customer_id'],
                                            $productId,
                                            $winDataTemp['amount']
                                        );
                                    }
                                }
                                $autoBid->setStatus(0);
                                $autoBid->setId($autoBid->getEntityId());
                                $this->saveObj($autoBid);
                            }
                        } elseif ($winDataTemp['type']== 'normal') {
                            $normalBidList = $this->_auctionAmount->create()->getCollection()
                                                        ->addFieldToFilter('product_id', ['eq' => $productId])
                                                        ->addFieldToFilter('auction_id', ['eq' => $bidId])
                                                        ->addFieldToFilter('status', ['eq' => 1]);

                            foreach ($normalBidList as $normalBid) {
                                if ($normalBid->getCustomerId() == $winDataTemp['customer_id']) {
                                    $normalBid->setWinningStatus(1);
                                    /** send notification mail to winner */
                                    if ($auctionConfig['enable_winner_notify_email']) {
                                        $this->_helperEmail->sendWinnerMail(
                                            $winDataTemp['customer_id'],
                                            $productId,
                                            $winDataTemp['amount']
                                        );
                                    }
                                }
                                $normalBid->setStatus(0);
                                $normalBid->setId($normalBid->getEntityId());
                                $this->saveObj($normalBid);
                            }
                        }

                        //save winner record in winner data table
                        $winnerDataModel = $this->_winnerData->create();
                        $auctionModel = $this->_auctionProductFactory->create()->load($bidId)->getData();
                        $auctionModel['customer_id'] = $winDataTemp['customer_id'];
                        $auctionModel['status'] = 1;
                        $auctionModel['auction_id'] = $auctionModel['entity_id'];
                        $auctionModel['win_amount'] = $winDataTemp['amount'];//$auctionModel['min_amount'];
                        unset($auctionModel['entity_id']);
                        $winnerDataModel->setData($auctionModel);
                       
                        $this->saveObj($winnerDataModel);
                        $auctionActPro->setAuctionStatus(0);
                    } else {
                        $auctionActPro->setAuctionStatus(2);
                    }

                    $auctionActPro->setId($auctionActPro->getEntityId());
                    
                    $this->saveObj($auctionActPro);
                    /***/
                } else {
                    $winnerDataModel = $this->_winnerData->create()->getCollection()
                                                    ->addFieldToFilter('product_id', ['eq' => $productId])
                                                    ->addFieldToFilter('status', ['eq'=>1])
                                                    ->getFirstItem();
                                                    
                    if ($winnerDataModel->getEntityId()) {
                        $bidDay = $winnerDataModel->getDays() ? 1: $winnerDataModel->getDays();
                        $current = strtotime($this->_dateTime->date('Y-m-d H:i:s'));
                        $day = strtotime($winnerDataModel->getStopBiddingTime().' + '.$bidDay.' days');
                        $difference = $day - $current;
                        if ($difference < 0) {
                            $winnerDataModel->setStatus(0);
                            $winnerDataModel->setId($winnerDataModel->getEntityId());
                            $this->saveObj($winnerDataModel);
                        }
                    }
                }
            }
        }
    }

    /**
     * saveObj
     * @param Object
     * @return void
     */
    private function saveObj($object)
    {
        $object->save();
    }
}
