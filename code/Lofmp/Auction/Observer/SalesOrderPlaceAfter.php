<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lofmp_Auction
 * @copyright  Copyright (c) 2017 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lofmp\Auction\Observer;;

use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Model\Session as CustomerSession;

class SalesOrderPlaceAfter implements ObserverInterface
{
    /**
     * @var CustomerSession
     */
    protected $_customerSession;

    /**
     * @var \Lofmp\Auction\Model\WinnerDataFactory
     */
    protected $_winnerData;

    /**
     * @var \Lofmp\Auction\Helper\Data
     */
    protected $_helperData;

    /**
     * @param CustomerSession $customerSession
     * @param Lofmp\Auction\Model\WinnerDataFactory
     * @param Lofmp\Auction\Model\Product
     * @param Lofmp\Auction\Helper\Data
     */
    public function __construct(
        CustomerSession $customerSession,
        \Lofmp\Auction\Model\WinnerDataFactory $winnerData,
        \Lofmp\Auction\Model\Product $auctionProduct,
        \Lofmp\Auction\Helper\Data $helperData
    ) {
    
        $this->_customerSession = $customerSession;
        $this->_winnerData = $winnerData;
        $this->_auctionProduct = $auctionProduct;
        $this->_helperData = $helperData;
    }

    /**
     * after place order event handler.
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $customerId = $this->_customerSession->getCustomerId();
        $order = $observer->getOrder();
        foreach ($order->getAllItems() as $item) {
            $activeAucId = $this->_helperData->getActiveAuctionId($item->getProductId());
            $auctionWinData = $this->_winnerData->create()->getCollection()
                                        ->addFieldToFilter('product_id', $item->getProductId())
                                        ->addFieldToFilter('status', 1)
                                        ->addFieldToFilter('complete', 0)
                                        ->addFieldToFilter('customer_id', $customerId)
                                        ->addFieldToFilter('auction_id', $activeAucId)
                                        ->getFirstItem();
            
            if ($auctionWinData->getEntityId()) {
                $winnerBidDetail = $this->_helperData->getWinnerBidDetail($auctionWinData->getAuctionId());
                if ($winnerBidDetail) {
                    //bider bid row update
                    $winnerBidDetail->setShop(1);
                    $this->saveObj($winnerBidDetail);
                    //update winner Data
                    $auctionWinData->setComplete(1);
                    $this->saveObj($auctionWinData);

                    $aucPro = $this->_auctionProduct->load($auctionWinData->getAuctionId());
                    //here we set auction process completely done
                    $aucPro->setAuctionStatus(4);
                    $aucPro->setStatus(1);
                    $this->saveObj($aucPro);
                }
            }
        }
        return $this;
    }

    /**
     * saveObj
     * @param Object
     * @return void
     */
    private function saveObj($object)
    {
        $object->save();
    }
}
