<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lofmp_Auction
 * @copyright  Copyright (c) 2017 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lofmp\Auction\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\CatalogInventory\Api\StockStateInterface;

class CheckoutCartProductAddAfter implements ObserverInterface
{
    /**
     * @var \Magento\Customer\Model\Session
     */

    protected $_customerSession;

    /**
     * @var \Magento\Checkout\Model\Session
     */

    protected $_checkoutSession;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */

    protected $_request;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */

    protected $_messageManager;

    /**
     * @var \Magento\CatalogInventory\Api\StockStateInterface
     */

    protected $_stockStateInterface;

    /**
     * @var \Lofmp\Auction\Model\WinnerDataFactory
     */

    protected $_winnerData;

    /**
     * @var \Lofmp\Auction\Model\ProductFactory
     */
    protected $_auctionProductFactory;

    /**
     * @param \Magento\Customer\Model\Session         $customerSession
     * @param \Magento\Checkout\Model\Session         $checkoutSession
     * @param RequestInterface                        $request
     * @param ManagerInterface                        $messageManager
     * @param StockStateInterface                     $stockStateInterface
     * @param \Lofmp\Auction\Model\ProductFactory        $auctionProductFactory
     * @param \Lofmp\Auction\Model\WinnerDataFactory $winnerData
     */
    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        RequestInterface $request,
        ManagerInterface $messageManager,
        StockStateInterface $stockStateInterface,
        \Lofmp\Auction\Model\WinnerDataFactory $winnerData,
         \Lofmp\Auction\Model\ProductFactory $auctionProductFactory,
        \Lofmp\Auction\Helper\Data $helperData
    ) {
    
        $this->_customerSession = $customerSession;
        $this->_checkoutSession = $checkoutSession;
        $this->_request = $request;
        $this->_messageManager = $messageManager;
        $this->_stockStateInterface = $stockStateInterface;
        $this->_winnerData = $winnerData;
        $this->_helperData = $helperData;
        $this->_auctionProductFactory = $auctionProductFactory;
    }

    /**
     * Sales quote add item event handler.
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
    	if ($this->_customerSession->isLoggedIn()) {
            //$price = '';
            $customerId = $this->_customerSession->getCustomerId();
            $item = $observer->getQuoteItem();
            $product = $item->getProduct();
            $productId = $product->getId();
            if ($product->getTypeId()=='configurable') {
                $superAttribute = $this->_request->getParam('super_attribute');
                if ($superAttribute) {
                    if (count($superAttribute)>0) {
                        //get product id according to attribute values
                        $pro = $product->getTypeInstance(true)->getProductByAttributes($superAttribute, $product);
                        $productId = $pro->getId();
                    }
                }
            }
            $biddingProductCollection = $this->_winnerData->create()->getCollection()
                                                    ->addFieldToFilter('product_id', ['eq' => $productId])
                                                    ->addFieldToFilter('status', ['eq' => 1])
                                                    ->addFieldToFilter('customer_id', ['eq' => $customerId])
                                                    ->addFieldToFilter('complete', ['eq' => 0])
                                                    ->setOrder('auction_id')->getFirstItem();

            if ($biddingProductCollection->getEntityId()) {
                $auctionId = $biddingProductCollection->getAuctionId();
                $auctionPro = $this->_auctionProductFactory->create()->load($auctionId);
                if ($auctionPro->getEntityId() && $auctionPro->getAuctionStatus() == 0) {
                    $bidWinPrice = $biddingProductCollection->getWinAmount();
                   
                    $item->setOriginalCustomPrice($bidWinPrice);
                    $item->setCustomPrice($bidWinPrice);
                    $item->getProduct()->setIsSuperMode(true);
                }
            }
        }
        if ($this->_customerSession->isLoggedIn()) {
            $maxqty=0;
            $mainArray=[];
            $customerId = $this->_customerSession->getCustomerId();
            $event = $observer->getQuoteItem();
            $product = $event->getProduct();
            $productId = $product->getId();
            $params = $this->_request->getParams();
            $cartData = $this->_checkoutSession->getQuote()->getAllItems();
            foreach ($cartData as $cart) {
                if ($cart->getProduct()->getId() == $params['product']) {
                    if ($product->getTypeId() == 'configurable') {
                        if (isset($params['super_attribute'])) {
                            if (count($params['super_attribute']) > 0) {
                                $pro = $product->getTypeInstance(true)
                                                ->getProductByAttributes($params['super_attribute'], $product);
                                $productId = $pro->getId();
                            }
                        }
                    }

                    // To Do -need to updated logic
                    $auctionId = $this->_helperData->getActiveAuctionId($productId);
                    $bidProCol = $this->_winnerData->create()->getCollection()
                                            ->addFieldToFilter('product_id', $productId)
                                            ->addFieldToFilter('complete', 0)->addFieldToFilter('status', 1)
                                            ->addFieldToFilter('customer_id', $customerId)
                                            ->addFieldToFilter('auction_id', $auctionId);

                    if (count($bidProCol)) {
                        foreach ($bidProCol as $winner) {
                            $maxqty+=$winner->getMaxQty();
                            $customerArray['max']=$winner->getMaxQty();
                            $customerArray['min']=$winner->getMinQty();
                            $mainArray[$winner->getCustomerId()]=$customerArray;
                        }
                    }
                    if (array_key_exists($customerId, $mainArray)) {
                        $customerMaxQty = $mainArray[$customerId]['max'];
                        $customerMinQty = $mainArray[$customerId]['min'];
                        $itemQty = $cart->getQty();

                        if ($itemQty > $customerMaxQty) {
                            $msg='Only '. $customerMaxQty.' quantities are allowed to purchase this item.';
                            $this->_messageManager->addNotice(__($msg));
                            $cart->setQty($customerMaxQty);
                            $this->saveObj($cart);
                        } elseif ($itemQty < $customerMinQty) {
                            $this->_messageManager
                                    ->addNotice(
                                        __('Atleast '. $customerMinQty .' quantities are allowed to purchase this item.')
                                    );
                            $cart->setQty($customerMinQty);
                            $this->saveObj($cart);
                        }
                    } else {
                        $stockQty = $this->_stockStateInterface->getStockQty($productId);
                        $availqty = $stockQty - $maxqty;
                        if ($availqty > 0) {
                            if ($cart->getQty() > $availqty) {
                                $this->_messageManager
                                        ->addNotice(
                                            __('You can add Only '. $availqty .' quantities to purchase this item.')
                                        );
                                $cart->setQty($availqty);
                                $this->saveObj($cart);
                            }
                        } else {
                            $this->_messageManager->addNotice(__('You can not add this quantities for purchase.'));
                            $cart->setQty($cart->getQty()-$params['qty']);
                            $this->saveObj($cart);
                        }
                    }
                }
            }
        }
        return $this;
    }

    /**
     * saveObj
     * @param Object
     * @return void
     */
    private function saveObj($object)
    {
        $object->save();
    }
}
