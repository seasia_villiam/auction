<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lofmp_Auction
 * @copyright  Copyright (c) 2017 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lofmp\Auction\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\CatalogInventory\Api\StockStateInterface;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;

class CheckoutCartUpdateItemsAfter implements ObserverInterface
{

    /**
     * @var \Magento\Customer\Model\Session
     */

    protected $_customerSession;

    /**
     * @var \Magento\Checkout\Model\Session
     */

    protected $_checkoutSession;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */

    protected $_request;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */

    protected $_messageManager;

    /**
     * @var \Magento\CatalogInventory\Api\StockStateInterface
     */

    protected $_stockStateInterface;

    /**
     * @var \Lofmp\Auction\Model\WinnerDataFactory
     */

    protected $_winnerData;

    /**
     * @var \Lofmp\Auction\Model\ProductFactory
     */

    protected $_auctionProductFactory;

    /**
     * @param \Magento\Customer\Model\Session         $customerSession
     * @param \Magento\Checkout\Model\Session         $checkoutSession
     * @param RequestInterface                        $request
     * @param ManagerInterface                        $messageManager
     * @param StockStateInterface                     $stockStateInterface
     * @param Configurable                            $configurableProTypeModel
     * @param \Lofmp\Auction\Model\WinnerDataFactory $winnerData
     * @param \Lofmp\Auction\Model\ProductFactory    $auctionProductFactory
     */
    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        RequestInterface $request,
        ManagerInterface $messageManager,
        StockStateInterface $stockStateInterface,
        Configurable $configurableProTypeModel,
        \Lofmp\Auction\Model\WinnerDataFactory $winnerData,
        \Lofmp\Auction\Model\ProductFactory $auctionProductFactory
    ) {
    
        $this->_customerSession = $customerSession;
        $this->_checkoutSession = $checkoutSession;
        $this->_request = $request;
        $this->_messageManager = $messageManager;
        $this->_stockStateInterface = $stockStateInterface;
        $this->_configurableProTypeModel = $configurableProTypeModel;
        $this->_winnerData = $winnerData;
        $this->_auctionProductFactory = $auctionProductFactory;
    }

    /**
     * Sales quote add item event handler.
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $maxqty='';
        $mainArray=[];
        if ($this->_customerSession->isLoggedIn()) {
            $cart = $this->_checkoutSession->getQuote()->getAllItems();
            $info = $observer->getInfo();
            $customerid = $this->_customerSession->getCustomerId();
            foreach ($cart as $item) {
                $proIds[]=$item->getProductId();
            }
            foreach ($cart as $item) {
                $product = $item->getProduct();
                $productId = $item->getProductId();
                if ($product->getTypeId() == 'configurable') {
                    $childPro=$this->_configurableProTypeModel
                                            ->getChildrenIds($product->getId());

                    $childProIds = isset($childPro[0]) ? $childPro[0]:[0];

                    $biddingCollection=$this->_auctionProductFactory->create()->getCollection()
                                                        ->addFieldToFilter('product_id', ['in'=>$childProIds])
                                                        ->addFieldToFilter('status', 1);
                    if (count($biddingCollection)) {
                        foreach ($biddingCollection as $bidProduct) {
                            $proId=$bidProduct->getProductId();
                            if (in_array($proId, $proIds)) {
                                $productId = $bidProduct->getProductId();
                                break;
                            }
                        }
                    }
                }
                $bidProCol = $this->_winnerData->create()->getCollection()
                                                            ->addFieldToFilter('product_id', $productId)
                                                            ->addFieldToFilter('status', 1)
                                                            ->addFieldToFilter('complete', 0)
                                                            ->setOrder('auction_id');
                if (count($bidProCol)) {
                    foreach ($bidProCol as $winner) {
                        $maxqty+=$winner->getMaxQty();
                        $customerArray['max']=$winner->getMaxQty();
                        $customerArray['min']=$winner->getMinQty();
                        $mainArray[$winner->getCustomerId()]=$customerArray;
                    }
                }
                if (array_key_exists($customerid, $mainArray)) {
                    $customerMaxQty=$mainArray[$customerid]['max'];
                    $customerMinQty=$mainArray[$customerid]['min'];
                    if ($item->getQty() <= $customerMaxQty && $item->getQty()>=$customerMinQty) {
                        $item->setQty($item->getQty());
                        $this->saveObj($item);
                    } elseif ($item->getQty() > $customerMaxQty) {
                        $item->setQty($customerMaxQty);
                        $this->saveObj($item);
                        $this->_messageManager
                                ->addNotice(
                                    __('Maximum '. $customerMaxQty .' quantities are allowed to purchase this item.')
                                );
                    } elseif ($item->getQty()<$customerMinQty) {
                        $item->setQty($customerMinQty);
                        $this->saveObj($item);
                        $this->_messageManager
                                ->addNotice(
                                    __('Minimum '. $customerMinQty .' quantities are allowed to purchase this item.')
                                );
                    }
                } else {
                    $stockQty = $this->_stockStateInterface->getStockQty($productId);
                    $availqty =  $stockQty-$maxqty;
                    if ($availqty > 0) {
                        if (array_key_exists('qty', $info[$item->getId()])) {
                            if ($info[$item->getId()]['qty']>=$availqty) {
                                $item->setQty($availqty);
                                $this->_messageManager
                                        ->addNotice(
                                            __('Maximum '. $availqty .' quantities are allowed to purchase this item.')
                                        );
                            } else {
                                $item->setQty($info[$item->getId()]['qty']);
                            }
                            $this->saveObj($item);
                        }
                    } else {
                        $item->setQty($item->getQty()-$params['qty']);
                        $this->saveObj($item);
                        $this->_messageManager->addNotice('you can not add this quantity for purchase.');
                    }
                }
            }
        }
        return $this;
    }

    /**
     * saveObj
     * @param Object
     * @return void
     */
    private function saveObj($object)
    {
        $object->save();
    }
}
