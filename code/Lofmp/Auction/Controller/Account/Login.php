<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lofmp_Auction
 * @copyright  Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lofmp\Auction\Controller\Account;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

class Login extends \Magento\Customer\Controller\AbstractAccount
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Directory\Model\CurrencyFactory
     */
    protected $_dirCurrencyFactory;


    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_dateTime;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $_timeZone;

    /**
     * @var \Lofmp\Auction\Model\AmountFactory
     */
    protected $_auctionAmount;

    /**
     * @var \Lofmp\Auction\Model\ProductFactory
     */
    protected $_auctionProductFactory;

    /**
     * @var \Lofmp\Auction\Helper\Data
     */
    protected $_helperData;
     /**
     * @var \Lof\MarketPlace\Helper\Data
     */
    protected $marketHelper;

    /**
     * @var \Lofmp\Auction\Model\AutoAuctionFactory
     */
    protected $_autoAuction;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Directory\Model\CurrencyFactory $dirCurrencyFactory
     * @param RequestInterface $request
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param \Lofmp\Auction\Model\AmountFactory $auctionAmount
     * @param \Lofmp\Auction\Model\ProductFactory $auctionProductFactory
     * @param \Lofmp\Auction\Helper\Data $helperData
     * @param \Lofmp\Auction\Model\AutoAuctionFactory $autoAuction
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Directory\Model\CurrencyFactory $dirCurrencyFactory,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        TimezoneInterface $timeZone,
        \Lofmp\Auction\Model\AmountFactory $auctionAmount,
        \Lofmp\Auction\Model\ProductFactory $auctionProductFactory,
        \Lofmp\Auction\Helper\Data $helperData,
        \Lofmp\Auction\Helper\Email $helperEmail,
        \Lofmp\Auction\Model\AutoAuctionFactory $autoAuction,
        \Lof\MarketPlace\Helper\Data $marketHelper
    ) {
        $this->marketHelper = $marketHelper;
        $this->_customerSession = $customerSession;
        $this->_resultPageFactory = $resultPageFactory;
        $this->_storeManager = $storeManager;
        $this->_dirCurrencyFactory = $dirCurrencyFactory;
        $this->_dateTime = $dateTime;
        $this->_timeZone = $timeZone;
        $this->_auctionAmount = $auctionAmount;
        $this->_auctionProductFactory = $auctionProductFactory;
        $this->_helperData = $helperData;
        $this->_helperEmail = $helperEmail;
        $this->_autoAuction = $autoAuction;
        parent::__construct($context);
    }

    /**
     * Check customer authentication
     *
     * @param RequestInterface $request
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function dispatch(RequestInterface $request)
    {
        $loginUrl = $this->_objectManager->get('Magento\Customer\Model\Url')->getLoginUrl();

        if (!$this->_customerSession->authenticate($loginUrl)) {
            $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
            //set Data in session if bidder not login
            $this->_customerSession->setAuctionBidData($request->getParams());
        }
        return parent::dispatch($request);
    }

    /**
     * Default customer account page
     * @var $cuntCunyCode current Currency Code
     * @var $baseCunyCode base Currency Code
     * @return \Magento\Backend\Model\View\Result\Redirect $resultRedirect
     */
    public function execute()
    {
        //get data from customerSession relared to Auction
        $data = $this->_customerSession->getAuctionBidData();
        $data = $data ? $data: $this->getRequest()->getParams();
        $auctionData = $this->_customerSession->getAuctionData();
        if(isset($data['data_auction'])) {
            $auctionData = json_decode($data['data_auction'],true);
        }
        //unset data of customerSession relared to Auction
        $this->_customerSession->unsAuctionBidData();
        $this->_customerSession->unsAuctionData();

        $today = $this->_timeZone->date()->format('m/d/y H:i:s');
        $difference = $auctionData['stop_auction_time_stamp'] - strtotime($today);
        if ($data && $difference > 0) {
            //auction configuration detail
            $auctionConfig = $this->_helperData->getAuctionConfiguration();

            //get currency according to store
            $store = $this->_storeManager->getStore();
            $currencyModel = $this->_dirCurrencyFactory->create();
            $baseCunyCode = $store->getBaseCurrencyCode();
            $cuntCunyCode = $store->getCurrentCurrencyCode();

            $allowedCurrencies = $currencyModel->getConfigAllowCurrencies();
            $rates = $currencyModel->getCurrencyRates(
                $baseCunyCode,
                array_values($allowedCurrencies)
            );

            $rates[$cuntCunyCode] = isset($rates[$cuntCunyCode]) ?
                                                 $rates[$cuntCunyCode] : 1;

            $data['auction_id'] = $auctionData['entity_id'];
            $data['product_id'] = $auctionData['product_id'];
            $data['pro_name'] = $auctionData['pro_name'];
            $data['bidding_amount'] = $data['bidding_amount']/$rates[$cuntCunyCode];

            if ($auctionConfig['auto_enable'] && $auctionData['auto_auction_opt']
                && array_key_exists("auto_bid_allowed", $data)) {
                $this->_saveAutobiddingAmount($data);
            } else {
                $this->_saveBiddingAmount($data);
            }
        } else {
            $this->messageManager->addError(__('Auction time expired...'));
        }

        if (!isset($auctionData['pro_url']) || $auctionData['pro_url']=='') {
            $auctionData['pro_url'] = $this->_url->getBaseUrl();
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setUrl($auctionData['pro_url']);
    }

    /**
     * _saveBiddingAmount saves amount of normal bid placed by customer
     * @param array $data stores bid data
     * @var int $val bid amount
     * @var int $userId current logged in customer's id
     * @var object $biddingModel holds data for particular bid
     * @var $minPrice int stores minimum price of bidding
     * @var $incopt stores increament option is allowed on bidding or not
     * @var $incPrice holds increment price for product
     * @var $bidCusrRecord object id customer already placed a bid
     * @var $bidModel use to strore new bidding
     */
    
    protected function _saveBiddingAmount($data)
    {
        $sendUserId = "";
        $customerArray = [];
        $val = $data['bidding_amount'];
        $auctionConfig = $this->_helperData->getAuctionConfiguration();
        $userId = $this->_customerSession->getCustomerId();
        $biddingModel = $this->_auctionProductFactory->create()->load($data['auction_id']);
        $minPrice = $biddingModel->getMinAmount();

        if ($biddingModel->getIncrementOpt() && $auctionConfig['increment_auc_enable']) {
            $incVal = $this->_helperData->getIncrementPriceAsRange($minPrice);
            $minPrice = $incVal ? $minPrice + $incVal : $minPrice;
        }
       
        if ($minPrice >= $val) {
            $this->messageManager->addError(__('You can not bid less than or equal to current bid price.'));
        } else {
            $bidCusrRecord = $this->_auctionAmount->create()->getCollection()
                                            ->addFieldToFilter('product_id', ['eq' => $data['product_id']])
                                            ->addFieldToFilter('customer_id', ['eq' => $userId])
                                            ->addFieldToFilter('auction_id', ['eq' => $data['auction_id']])
                                            ->addFieldToFilter('status', ['eq' => '1'])->getFirstItem();
            if ($bidCusrRecord->getEntityId()) {
                $bidCusrRecord->setId($bidCusrRecord->getEntityId());
                $bidCusrRecord->setAuctionAmount($data['bidding_amount']);
                $bidCusrRecord->setCreatedAt($this->_dateTime->date('Y-m-d H:i:s'));
                $bidCusrRecord->save();
            } else {
                $bidModel = $this->_auctionAmount->create();
                $bidModel->setAuctionAmount($data['bidding_amount']);
                $bidModel->setCustomerId($userId);
                $bidModel->setProductId($data['product_id']);
                $bidModel->setAuctionId($data['auction_id']);
                $seller_id = $this->marketHelper->getSellerIdByProduct($data['product_id']);
                if($seller_id) {
                    $bidModel->setSellerId($seller_id);
                }
                $bidModel->setStatus(1);
                $bidModel->save();
            }
          
            if ($auctionConfig['enable_admin_email']) {
                $this->_helperEmail->sendMailToAdmin($userId, $data['product_id'], $data['bidding_amount']);
            }
            $autoBidList = $this->_autoAuction->create()->getCollection()
                                        ->addFieldToFilter('auction_id', ['eq' => $data['auction_id']])
                                        ->addFieldToFilter('status', ['eq' => '1']);
            if (count($autoBidList) && $auctionConfig['enable_outbid_email']) {
                $autoBidArray = [];
                foreach ($autoBidList as $autoBidData) {
                    $autoBidArray[$autoBidData['customer_id']] = $autoBidData['amount'];
                }
                arsort($autoBidArray);
                if ($val > max($autoBidArray)) {
                    $sendUserId = 0;
                    foreach ($autoBidArray as $buyr => $amnt) {
                        $sendUserId = $buyr;
                        break;
                    }
                    if ($sendUserId != 0 && $sendUserId != $userId) {
                        $this->_helperEmail->sendOutBidAutoBidder($sendUserId, $userId, $data['product_id']);
                    }
                }
            }

            if ($auctionConfig['enable_outbid_email']) {
                $listForSendMail = $this->_auctionAmount->create()->getCollection()
                                            ->addFieldToFilter('product_id', ['eq' => $data['product_id']])
                                            ->addFieldToFilter('auction_id', ['eq' => $data['auction_id']]);
                                            /*->addFieldToFilter(
                                                'status', ['eq' => '1']
                                            );*/

                /**
                 * @var int $customerId previous bidder customer
                 * @var int $userId current bidder user
                 */
                foreach ($listForSendMail as $key) {
                    $custmerId = $key->getCustomerId();
                    if ($custmerId != $userId && $custmerId != $sendUserId) {
                        $this->_helperEmail->sendMailToMembers($custmerId, $userId, $data['product_id']);
                    }
                }
            }
            $biddingModel->setMinAmount($val);
            $biddingModel->save();
            $this->messageManager->addSuccess(__('Your bid amount successfully saved..'));
        }
    }

    /**
     * _saveAutobiddingAmount calls to store auto bid placed by customer
     * @param array $data holda data related to bidding
     * @var $userId int holds current customer id
     * @var $biddingModel object stores bidding details
     * @var $auctionConfig['auto_use_increment'] int stores whether increment option is enable in admin panel or not
     * @var $auctionConfig['auto_auc_limit'] stores whether customer can place auto bid multiple times or not
     * @var $minPrice int stores minimum price of bidding
     * @var $incopt stores increament option is allowed on bidding or not
     * @var $incprice holds increment price for product
     * @var $pid int product id on which bid is placed
     * @var $val int bidding amount placed by customer
     * @var $autoBidRecord object checks whether there is already bid already exist for current customer or not
     * @var $autoBidModel autobid model to store auto bid
     * @var $listToSendMail use to get maximum auto bid amount
     */

    protected function _saveAutobiddingAmount($data)
    {
        $val = $data['bidding_amount'];
        $userId = $this->_customerSession->getCustomerId();
        $auctionConfig = $this->_helperData->getAuctionConfiguration();
        $biddingModel = $this->_auctionProductFactory->create()->load($data['auction_id']);
        $changeprice = $minPrice = $biddingModel->getMinAmount();

        $autoBidRecordList = $this->_autoAuction->create()->getCollection()
                                    ->addFieldToFilter('auction_id', ['eq' => $data['auction_id']])
                                    ->addFieldToFilter('status', ['eq' => '1'])
                                    ->addFieldToFilter('amount', ['gteq' => $val])->getFirstItem();

        if ($biddingModel->getIncrementOpt() && $auctionConfig['auto_use_increment']) {
            $incVal = $this->_helperData->getIncrementPriceAsRange($minPrice);
            $changeprice = $minPrice = $incVal ? $minPrice + $incVal : $minPrice;

            $incVal = $this->_helperData->getIncrementPriceAsRange($minPrice);
            $minPrice = $incVal ? $minPrice + $incVal : $minPrice;
        }

        if ($minPrice >= $val || $autoBidRecordList->getEntityId()) {
            $this->messageManager->addError(__('You can not auto bid less than or equal to current bid price.'));
        } else {
            $autoBidRecord = $this->_autoAuction->create()->getCollection()
                                    ->addFieldToFilter('customer_id', ['eq' => $userId])
                                    ->addFieldToFilter('auction_id', ['eq' => $data['auction_id']])
                                    ->addFieldToFilter('status', ['eq' => '1'])->getFirstItem();
            if ($autoBidRecord->getEntityId()) {
                if (!$auctionConfig['auto_auc_limit']) {
                    $this->messageManager->addError(__('You are not allowed to auto bid again.'));
                    return;
                } else {
                    $autoBidRecord->setId($autoBidRecord->getEntityId());
                    $autoBidRecord->setAmount($data['bidding_amount']);
                    $autoBidRecord->setCreatedAt($this->_dateTime->date('Y-m-d H:i:s'));
                    $autoBidRecord->save();
                }
            } else {
                $autoBidModel = $this->_autoAuction->create();
                $autoBidModel->setAmount($data['bidding_amount']);
                $autoBidModel->setCustomerId($userId);
                $autoBidModel->setProductId($data['product_id']);
                $seller_id = $this->marketHelper->getSellerIdByProduct($data['product_id']);
                if($seller_id) {
                    $autoBidModel->setSellerId($seller_id);
                }
                $autoBidModel->setAuctionId($data['auction_id']);
                $autoBidModel->setStatus(1);
                $autoBidModel->save();
            }

            $maxar=[0];
            $listToSendMail = $this->_autoAuction->create()->getCollection()
                                            ->addFieldToFilter('auction_id', ['eq' => $data['auction_id']])
                                            ->addFieldToFilter('status', ['eq' => '1']);
            foreach ($listToSendMail as $ky) {
                array_push($maxar, $ky->getAmount());
            }
            $max = max($maxar);
            if ($auctionConfig['enable_outbid_email'] && $max<=$val) {
                foreach ($listToSendMail as $ky) {
                    $custmerId ='';
                    if ($ky->getAmount()<=$val) {
                        $custmerId = $ky->getCustomerId();
                        if ($userId != $custmerId) {
                            $this->_helperEmail->sendAutoMailUsers($custmerId, $userId, $data['product_id']);
                        }
                    }
                }
            }

            if ($auctionConfig['enable_admin_email']) {
                $this->_helperEmail->sendAutoMailToAdmin($userId, $data['product_id'], $data['bidding_amount']);
            }

            if ($auctionConfig['enable_auto_outbid_msg'] && $max > $val) {
                $this->messageManager->addError(__($auctionConfig['show_auto_outbid_msg']));
            } else {
                $biddingModel->setMinAmount($changeprice);
                $biddingModel->save();
                $this->messageManager->addSuccess(__('Your auto bid amount successfully saved'));
            }
            return ;
        }
    }
}
