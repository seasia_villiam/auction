<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lofmp_Auction
 * @copyright  Copyright (c) 2017 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */
namespace Lofmp\Auction\Controller\Adminhtml\Product;

use Magento\Backend\App\Action;
use Lofmp\Auction\Model\ProductFactory;
use Magento\MediaStorage\Model\File\UploaderFactory;

class Save extends \Magento\Backend\App\Action
{
    /**
     * Core registry.
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;
    /**
     * @var Lofmp\Auction\Model\Product
     */
    protected $_auction;
    /**
     * @var Magento\MediaStorage\Model\File\UploaderFactory
     */
    protected $_fileUploader;
    /**
     * @var \Magento\Framework\File\Csv
     */
    protected $_csvReader;

    /**
     * @param Action\Context                             $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Registry                $registry
     */
    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        ProductFactory $auction,
        UploaderFactory $fileUploader,
        \Magento\Framework\File\Csv $csvReader
    ) {
        parent::__construct($context);
        $this->_resultPageFactory = $resultPageFactory;
        $this->_auction = $auction;
        $this->_fileUploader = $fileUploader;
        $this->_csvReader = $csvReader;
    }

    /**
     * Check for is allowed.
     *
     * @return bool
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
    
        if ($this->getRequest()->isPost()) {
            try {
                if(isset($_FILES['import_file'])) {
                    if (!$this->_formKeyValidator->validate($this->getRequest())) {
                        return $this->resultRedirectFactory->create()->setPath('*/*/index');
                    }
                  
                    $uploader = $this->_fileUploader->create(
                        ['fileId' => 'import_file']
                    );
                     
                    $result = $uploader->validateFile();
                   
                    $file = $result['tmp_name'];
                    $fileNameArray = explode('.', $result['name']);
                    
                    $ext = end($fileNameArray);
                    if ($file != '' && $ext == 'csv') {
                        $csvFileData = $this->_csvReader->getData($file);
                        $count = 0;
                        foreach ($csvFileData as $key => $rowData) {
                            if (count($rowData) < 9 && $count == 0) {
                                $this->messageManager->addError(__('Csv file is not a valid file!'));
                                return $this->resultRedirectFactory->create()->setPath('*/*/index');
                            }
                             if ($rowData[0] == '' ||
                                $rowData[1] == '' ||
                                $rowData[2] == '' ||
                                $rowData[3] == '' ||
                                $count == 0
                            ) {
                                ++$count;
                                continue;
                            }
                      
                            $auction = $this->_auction->create();
                            $auction->addFieldToFilter('proudtc_id', $rowData[0]);
                            if(count($auction) == 0) {
                                $auction->setData($rowData)->save();
                            }
                            if (($count - 1) > 1) {
                                $this->messageManager->addNotice(__('Some rows are not valid!'));
                            }
                            if (($count - 1) <= 1) {
                                $this->messageManager
                                    ->addSuccess(
                                        __('Your auction detail has been successfully Saved')
                                    );
                            }
                            return $this->resultRedirectFactory->create()->setPath('*/*/index');
                        }
                    }
                }
            }
        }
    }
}