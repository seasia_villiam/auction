<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://www.landofcoder.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lofmp_Auction
 * @copyright  Copyright (c) 2017 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */
namespace Lofmp\Auction\Controller\Adminhtml\Product;

use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Lofmp\Auction\Model\ProductFactory;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var Lofmp\Auction\Model\Product
     */
    protected $_auction;
    /**
     * @param Action\Context $context
     */
    public function __construct(
        Action\Context $context,
        \Magento\Framework\Filesystem $filesystem, 
        \Magento\Backend\Helper\Js $jsHelper,
        UploaderFactory $fileUploader,
        \Magento\Framework\File\Csv $csvReader,
         ProductFactory $auction
         )
    {
         $this->_fileSystem = $filesystem;
          $this->jsHelper = $jsHelper;
            $this->_fileUploader = $fileUploader;
        $this->_csvReader = $csvReader;
         $this->_auction = $auction;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
    	return $this->_authorization->isAllowed('Lofmp_Auction::product_save');
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
    	$data = $this->getRequest()->getPostValue();
    	/** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
    	$resultRedirect = $this->resultRedirectFactory->create(); 
    	if ($data) {
            if(isset($_FILES['import_file'])) {
                if (!$this->_formKeyValidator->validate($this->getRequest())) {
                    return $this->resultRedirectFactory->create()->setPath('*/*/index');
                }
              
                $uploader = $this->_fileUploader->create(
                    ['fileId' => 'import_file']
                );
                 
                $result = $uploader->validateFile();
               
                $file = $result['tmp_name'];
                $fileNameArray = explode('.', $result['name']);
                
                $ext = end($fileNameArray);
                if ($file != '' && $ext == 'csv') {
                    $csvFileData = $this->_csvReader->getData($file);
                    
                    $count = 0;
                    $csvData = array();
                    foreach ($csvFileData as $key => $rowData) {
                        if (count($rowData) < 18 && $count == 0) {
                            $this->messageManager->addError(__('Csv file is not a valid file!'));
                            return $this->resultRedirectFactory->create()->setPath('*/*/index');
                        }
                         if ($rowData[0] == '' ||
                            $rowData[1] == '' ||
                            $rowData[2] == '' ||
                            $rowData[3] == '' ||
                            $count == 0
                        ) {
                            ++$count;
                            continue;
                        }
                        $csvData['product_id'] = $rowData[0];
                        $csvData['customer_id'] = $rowData[1];
                        $csvData['min_amount'] = $rowData[2];
                        $csvData['starting_price'] = $rowData[3];
                        $csvData['reserve_price'] = $rowData[4];
                        $csvData['auction_status'] = $rowData[5];
                        $csvData['days'] = $rowData[6];
                        $csvData['min_qty'] = $rowData[7];
                        $csvData['max_qty'] = $rowData[8];
                        $csvData['start_auction_time'] = $rowData[9];
                        $csvData['stop_auction_time'] = $rowData[10];
                        $csvData['increment_opt'] = $rowData[11];
                        $csvData['increment_price'] = $rowData[12];
                        $csvData['auto_auction_opt'] = $rowData[13];
                        $csvData['status'] = $rowData[14];
                        $csvData['created_at'] = $rowData[15];
                        $csvData['buy_it_now'] = $rowData[16];
                        $csvData['featured_auction'] = $rowData[17];

                        
                        $auction = $this->_auction->create();
                        $count_auction = $auction->getCollection()->addFieldToFilter('product_id', $rowData[0]);
                       
                        if(count($count_auction) == 0) {
                          
                            $auction->setData($csvData)->save();
                              
                        }
                      
                    }
                    if (($count - 1) > 1) {
                            $this->messageManager->addNotice(__('Some rows are not valid!'));
                    }
                    if (($count - 1) <= 1) {
                        $this->messageManager
                            ->addSuccess(
                                __('Your auction detail has been successfully Saved')
                            );
                    }
                    return $this->resultRedirectFactory->create()->setPath('*/*/index');
                }
            } else {

        		$model = $this->_objectManager->create('Lofmp\Auction\Model\Product');

        		$id = $this->getRequest()->getParam('id');

                if ($id) {
                    $model->load($id);
                } else {
                     $model->setAuctionStatus(1);
                     $model->setStatus(0);
                }
                  if(isset($data['product_id']) && !is_numeric($data['product_id'])) {
                    $data['product_id'] = str_replace('product/','',$data['product_id']);
                }
                $model->setData($data);

                try {
                    $model->save();
                    $this->messageManager->addSuccess(__('You saved this product.'));
                    $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                    if ($this->getRequest()->getParam('back')) {
                        return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId(), '_current' => true]);
                    }
                    return $resultRedirect->setPath('*/*/');
                } catch (\Magento\Framework\Exception\LocalizedException $e) {
                    $this->messageManager->addError($e->getMessage());
                } catch (\RuntimeException $e) {
                    $this->messageManager->addError($e->getMessage());
                } catch (\Exception $e) {
                    $this->messageManager->addException($e, __('Something went wrong while saving the seller.'));
                }

                $this->_getSession()->setFormData($data);
                return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
            }
        }
        return $resultRedirect->setPath('*/*/');
    }
}