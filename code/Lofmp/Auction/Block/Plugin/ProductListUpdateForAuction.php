<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lofmp_Auction
 * 
 * @copyright  Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */
namespace Lofmp\Auction\Block\Plugin;

class ProductListUpdateForAuction
{
    /**
     * @var \Lofmp\Auction\Helper\Data
     */
    protected $helperData;

    public function __construct(\Lofmp\Auction\Helper\Data $helperData)
    {
        $this->helperData = $helperData;
    }

    public function aroundGetProductPrice(\Magento\Catalog\Block\Product\ListProduct $list, $proceed, $product)
    {
        $auctionDetail = $this->helperData->getProductAuctionDetail($product);
        return $proceed($product).$auctionDetail;
    }
}
