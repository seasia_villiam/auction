<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lof_Auction
 * @copyright  Copyright (c) 2017 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lofmp\Auction\Block\Bid;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
class All extends \Magento\Framework\View\Element\Template {

      protected $_template = 'Lofmp_Auction::auction/all.phtml';
    
	/**
     * @var \Lofmp\Auction\Helper\Data
     */
	protected $helper;
	/**
     * @var \Lofmp\Auction\Model\Product
     **/
	protected $auctionProduct;

	/**
	 * @param \Magento\Framework\View\Element\Template\Context
	 * @param \Lofmp\Auction\Helper\Data
	 * @param array
	 */
	public function __construct(
		\Magento\Catalog\Block\Product\Context $context,
		\Lofmp\Auction\Helper\Data $helper,
        CustomerSession $customerSession,
		\Magento\Framework\Pricing\PriceCurrencyInterface $priceHelper,
		\Lofmp\Auction\Model\Product $auctionProduct,
		\Lofmp\Auction\Model\AmountFactory $aucAmountFactory,
		\Lofmp\Auction\Model\AutoAuctionFactory $autoAuctionFactory,
        \Lofmp\Auction\Model\ProductFactory $auctionProFactory,
		array $data = []
		) {
        $this->_customerSession = $customerSession;
         $this->_coreRegistry = $context->getRegistry();
          $this->_auctionProFactory = $auctionProFactory;
        $this->_aucAmountFactory = $aucAmountFactory;		
		$this->auctionProduct = $auctionProduct;
		$this->helper = $helper;
		$this->_autoAuctionFactory = $autoAuctionFactory;
		 $this->_priceHelper = $priceHelper;
		parent::__construct($context, $data);
	}

    /**
     * For get Bidding record count
     * @param int $auctionId
     * @return string
     */
    public function getNumberOfBid($auctionId)
    {
        $records = $this->_aucAmountFactory->create()->getCollection()
                                    ->addFieldToFilter('auction_id', ['eq'=>$auctionId]);
        return count($records).__(' Bids');
    }
	 /**
     * @var $productList Product list of current page
     * @return array of current category product in auction and buy it now
     */
    public function getHistory($product_id) {
        $aucAmtData = $this->_aucAmountFactory->create()->getCollection()
            ->addFieldToFilter('product_id', ['eq' => $product_id])
            ->addFieldToFilter('status', ['eq'=>1])
            ->setOrder('auction_amount','DESC');
        return  $aucAmtData;   
    }
     /**
     * @var $productList Product list of current page
     * @return array of current category product in auction and buy it now
     */
    public function getHistoryAuto($customerId,$product_id) {
        $aucAmtData = $this->_autoAuctionFactory->create()->getCollection()
            ->addFieldToFilter('product_id', ['eq' => $product_id])
            ->addFieldToFilter('customer_id',$customerId)
            ->addFieldToFilter('status', ['eq'=>1])
            ->getFirstItem();
        return  $aucAmtData;   
    }
	   /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
     /**
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($this->getAuction()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'sales.order.history.pager'
            )->setCollection(
                $this->getAuction()
            );
            $this->setChild('pager', $pager);
            $this->getAuction()->load();
        }
        return $this;
    }

  public function getAuction() {
    $now = new \DateTime();
    return $this->auctionProduct->getCollection()->addFieldToFilter('stop_auction_time', ['gteq' => $now->format('Y-m-d H:i:s')]);
  }
    public function getAuctionFeatured() {
        return $this->getAuction()->addFieldToFilter('featured_auction',1);
    }
	public function getToday() {
		return $this->_localeDate->date()->format('m/d/y H:i:s');
	}
    /**
     * @var $productList Product list of current page
     * @return array of current category product in auction and buy it now
     */
    public function getAuctionDetail($currentProId)
    {
        $auctionConfig = $this->getAuctionConfiguration();
        $auctionData = false;
        $curPro = $this->helper->getProductById($currentProId);
        if ($auctionConfig['enable']) {
           
          
            $auctionData = $this->_auctionProFactory->create()->getCollection()
                                    ->addFieldToFilter('product_id', ['eq'=>$currentProId])
                                    ->addFieldToFilter('auction_status', ['in' => [0,1]])
                                    ->addFieldToFilter('status', ['eq'=>0])->getFirstItem()->getData();

            if (isset($auctionData['entity_id'])) {
                if ($auctionData['increment_opt'] && $auctionConfig['increment_auc_enable']) {
                    $incVal = $this->helper->getIncrementPriceAsRange($auctionData['min_amount']);
                    $auctionData['min_amount'] = $incVal ? $auctionData['min_amount'] + $incVal
                                                                        : $auctionData['min_amount'];
                }
                
                
                $aucAmtData = $this->_aucAmountFactory->create()->getCollection()
                                        ->addFieldToFilter('product_id', ['eq' => $currentProId])
                                        ->addFieldToFilter('auction_id', ['eq'=> $auctionData['entity_id']])
                                        ->addFieldToFilter('winning_status', ['eq'=>1])
                                        ->addFieldToFilter('shop', ['neq'=>0])->getFirstItem();

                if ($aucAmtData->getEntityId()) {
                    $aucAmtData = $this->_autoAuctionFactory->create()->getCollection()
                                            ->addFieldToFilter('auction_id', ['eq'=> $auctionData['entity_id']])
                                            ->addFieldToFilter('flag', ['eq'=>1])
                                            ->addFieldToFilter('shop', ['neq'=>0])->getFirstItem();
                }

                $today = $this->_localeDate->date()->format('m/d/y H:i:s');
                $auctionData['stop_auction_time'] = $this->_localeDate
                                                            ->date(
                                                                new \DateTime($auctionData['stop_auction_time'])
                                                            )->format('Y-m-d H:i:s');
                $auctionData['start_auction_time'] = $this->_localeDate
                                                            ->date(
                                                                new \DateTime($auctionData['start_auction_time'])
                                                            )->format('Y-m-d H:i:s');

                $auctionData['current_time_stamp'] = strtotime($today);
                $auctionData['start_auction_time_stamp'] = strtotime($auctionData['start_auction_time']);
                $auctionData['stop_auction_time_stamp'] = strtotime($auctionData['stop_auction_time']);
                $auctionData['new_auction_start'] = $aucAmtData->getEntityId() ? true : false;
                $auctionData['auction_title'] = __('Bid on ').$curPro->getName();
               // $auctionData['pro_url'] = $curPro->getProductUrl();
                $auctionData['pro_url'] = $this->getUrl().$curPro->getUrlKey().'.html';
                $auctionData['pro_name'] = $curPro->getName();
                $auctionData['pro_buy_it_now'] = $auctionData['buy_it_now'];
                $auctionData['pro_auction'] = $auctionData['auction_status'];
                if ($auctionData['min_amount'] < $auctionData['starting_price']) {
                    $auctionData['min_amount'] = $auctionData['starting_price'];
                }
            } else {
                $auctionData = false;
            }
            
        }

        $this->_customerSession->setAuctionData($auctionData);
        return $auctionData;
    }
      /**
     * @return array of Auction Configuration Settings
     */
    public function getAuctionConfiguration()
    {
        return $this->helper->getAuctionConfiguration();
    }

     /**
     * @return string url of auction form
     */

    public function getAuctionFormAction()
    {
        $curPro = $this->_coreRegistry->registry('current_product');
        if ($curPro) {
            $currentProId = $curPro->getEntityId();
        } else {
            $auctionId = $this->getRequest()->getParam('id');
            $currentProId = $this->getAuctionProId($auctionId);
        }
        return $this->_customerSession->isLoggedIn() ?
                        $this->_urlBuilder->getUrl("lofauction/account/login"):
                        $this->_urlBuilder->getUrl('lofauction/account/login/', ['id'=>$currentProId]);
    }
	/**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function getLofProductPriceHtml(
        \Magento\Catalog\Model\Product $product,
        $priceType = null,
        $renderZone = \Magento\Framework\Pricing\Render::ZONE_ITEM_LIST,
        array $arguments = []
    ) {
        if (!isset($arguments['zone'])) {
            $arguments['zone'] = $renderZone;
        }
        $arguments['price_id'] = isset($arguments['price_id'])
            ? $arguments['price_id']
            : 'old-price-' . $product->getId() . '-' . $priceType;
        $arguments['include_container'] = isset($arguments['include_container'])
            ? $arguments['include_container']
            : true;
        $arguments['display_minimal_price'] = isset($arguments['display_minimal_price'])
            ? $arguments['display_minimal_price']
            : true;

            /** @var \Magento\Framework\Pricing\Render $priceRender */
        $priceRender = $this->getLayout()->getBlock('product.price.render.default');

        $price = '';
        if ($priceRender) {
            $price = $priceRender->render(
                \Magento\Catalog\Pricing\Price\FinalPrice::PRICE_CODE,
                $product,
                $arguments
            );
        }
        return $price;
    }
     /**
     * get currency in format
     * @param $amount float
     * @return string
     *
     */
    public function formatPrice($amount)
    {
        return $this->_priceHelper->convertAndFormat($amount);
    }

    
}