<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lofmp_Auction
 * @copyright  Copyright (c) 2017 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lofmp\Auction\Block\Customer;

class TopLink extends \Magento\Framework\View\Element\Html\Link {

   
    
	/**
     * @var \Lofmp\Auction\Helper\Data
     */
	protected $helper;
	/**
     * @var \Lofmp\Auction\Model\Product
     **/
	protected $auctionProduct;

	/**
	 * @param \Magento\Framework\View\Element\Template\Context
	 * @param \Lofmp\Auction\Helper\Data
	 * @param array
	 */
	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Lofmp\Auction\Helper\Data $helper,
		\Lofmp\Auction\Model\Product $auctionProduct,
		array $data = []
		) {
		$this->auctionProduct = $auctionProduct;
		$this->helper = $helper;
		parent::__construct($context, $data);
	}

	public function getHref() {
		$url = $this->getUrl ( 'lofmpauction/bid/index' );
		return $url;
	}
	
	public function getAuction() {
		return $this->auctionProduct->getCollection();
	}
	/**
     * Render block HTML
     *
     * @return string
     */
	protected function _toHtml()
	{	

		return '<li><a href="' . $this->getHref() . '"> ' . $this->getLabel() . ' </a></li>';
	}

	 /**
     * Function to Get Label on Top Link
     *
     * @return string
     */
    public function getLabel() {
        return __ ( 'Auctions');
    }
    
}