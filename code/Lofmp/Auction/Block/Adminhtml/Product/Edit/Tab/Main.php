<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lofmp_Auction
 * @copyright  Copyright (c) 2017 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lofmp\Auction\Block\Adminhtml\Product\Edit\Tab;

use \Magento\Backend\Block\Widget\Tab\TabInterface;

class Main extends \Magento\Backend\Block\Widget\Form\Generic implements TabInterface
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;
     /**
     * @var \Lofmp\Auction\Model\Config\Source\MethodTypes 
     */
    protected $methodTypes;

    protected $_productloader;
     /**
     * @var AllProductsForAuction
     */
    protected $_allProducts;
        /**
     * @var array configuration of Auction
     */

    protected $helper;
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Magento\Catalog\Model\ProductFactory $_productloader,
        \Lofmp\Auction\Model\ResourceModel\Product\Source\AllProductsForAuction $allProducts,
        \Lofmp\Auction\Helper\Data $helper,
        array $data = []
    ) {
        $this->helper = $helper;
        $this->_allProducts = $allProducts;
        $this->_systemStore = $systemStore;
        $this->_productloader = $_productloader;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('lofmpauction_product');
        $dateFormat = $this->_localeDate->getDateFormat(\IntlDateFormatter::SHORT);
        if ($this->_isAllowedAction('Lofmp_Auction::product')) {
            $isElementDisabled = false;
        } else {
            $isElementDisabled = true;
        }

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        //$form->setUseContainer(true);

        $fieldset = $form->addFieldset(
            'options_fieldset',
            ['legend' => __('Product Information'), 'class' => 'fieldset-wide fieldset-widget-options']
        );
        if ($model->getId()) {
            $fieldset->addField('entity_id', 'hidden', ['name' => 'entity_id']);
        }
     
        if(!$model->getProductId()) {
            $chooserField = $fieldset->addField(
                'options_fieldset_entity_id',
                'label',
                [
                    'name' => 'product_id',
                    'label' => __('Product'),
                    'required' => true,
                    'class' => 'widget-option',
                    'note' => __("Select a product"),
                ]
            );
            /*Add chooser helper for the field*/
            $helperData  = [
                'data' => [
                    'button' => ['open' => __("Select Product...")]
                ]
            ];
            $chooserField->setValue('product/'.$model->getProductId());
            $helperBlock = $this->getLayout()->createBlock(
                'Magento\Catalog\Block\Adminhtml\Product\Widget\Chooser',
                '',
                $helperData
            );

            $helperBlock->setConfig($helperData)
                ->setFieldsetId($fieldset->getId())
                ->prepareElementHtml($chooserField)
                ->setValue($model->getId());
        } else {
             $fieldset->addField(
            'product_id',
            'note',
            [
                'name' => 'product_id',
                'label' => __('Product'),
                'title' => __('Product'),
                'text' => $this->helper->getProductbyId($model->getProductId())->getName()
            ]
            );
        }    
        $fieldset->addField(
            'starting_price',
            'text',
            [
                'name' => 'starting_price',
                'label' => __('Starting Price'),
                'id' => 'starting_price',
                'title' => __('Starting Price'),
                'type' => 'price',
                'class' => 'required-entry validate-zero-or-greater',
                'required' => true,
            ]
        );
        $reqClass = "";
        $requiredOpt = false;
        if ($this->helper->getAuctionConfiguration()['reserve_enable']) {
            $reqClass = 'required-entry';
            $requiredOpt = true;
        }
        $fieldset->addField(
            'reserve_price',
            'text',
            [
                'name' => 'reserve_price',
                'label' => __('Reserve Price'),
                'id' => 'reserve_price',
                'title' => __('Reserve Price'),
                'values' => [],
                'class' => $reqClass.' validate-zero-or-greater',
                'required' => $requiredOpt,
            ]
        );
        
        $fieldset->addField(
            'start_auction_time',
            'date',
            [
                'name' => 'start_auction_time',
                'label' => __('Start Auction Time'),
                'id' => 'start_auction_time',
                'title' => __('Start Auction Time'),
                'date_format' => $dateFormat,
                'time_format' => 'HH:mm:ss',
                'class' => 'required-entry admin__control-text',
                'style' => 'width:210px',
                'required' => true,
            ]
        );
        $fieldset->addField(
            'stop_auction_time',
            'date',
            [
                'name' => 'stop_auction_time',
                'label' => __('Stop Auction Time'),
                'id' => 'stop_auction_time',
                'title' => __('Stop Auction Time'),
                'date_format' => $dateFormat,
                'time_format' => 'HH:mm:ss',
                'class' => 'required-entry admin__control-text',
                'style' => 'width:210px',
                'required' => true,
            ]
        );

        $fieldset->addField(
            'days',
            'text',
            [
                'name' => 'days',
                'label' => __('Number of Days Till Winner Can Buy'),
                'id' => 'days',
                'title' => __('Number of Days Till Winner Can Buy'),
                'class' => 'required-entry integer validate-zero-or-greater',
                'required' => true,
            ]
        );
        $fieldset->addField(
            'min_qty',
            'text',
            [
                'name' => 'min_qty',
                'label' => __('Minimum Quantity'),
                'id' => 'min_qty',
                'title' => __('Minimum Quantity'),
                'class' => 'required-entry validate-zero-or-greater',
                'required' => true,
            ]
        );

        $fieldset->addField(
            'max_qty',
            'text',
            [
                'name' => 'max_qty',
                'label' => __('Maximum Quantity'),
                'id' => 'max_qty',
                'title' => __('Maximum Quantity'),
                'class' => 'required-entry validate-zero-or-greater',
                'required' => true,
            ]
        );

        if ($this->helper->getAuctionConfiguration()['increment_auc_enable']) {
            $fieldset->addField(
                'increment_opt',
                'select',
                [
                    'name' => 'increment_opt',
                    'label' => __('Increment Option'),
                    'options' => ['1' => __('Enabled'), '0' => __('Disabled')],
                    'id' => 'increment_opt',
                    'title' => __('Increment Option'),
                    'class' => 'required-entry',
                    'required' => true,
                ]
            );
        }
        
        $fieldset->addField(
            'auto_auction_opt',
            'select',
            [
                'name' => 'auto_auction_opt',
                'label' => __('Automatic Option'),
                'options' => ['1' => __('Enabled'), '0' => __('Disabled')],
                'id' => 'auto_auction_opt',
                'title' => __('Automatic Option'),
                'class' => 'required-entry',
                'required' => true,
            ]
        );
        
         $fieldset->addField(
            'buy_it_now',
            'select',
            [
                'name' => 'buy_it_now',
                'label' => __('Buy It Now'),
                'options' => ['1' => __('Enabled'), '0' => __('Disabled')],
                'id' => 'buy_it_now',
                'title' => __('Buy It Now'),
                'class' => 'required-entry',
                'required' => true,
            ]
        );
         $fieldset->addField(
            'featured_auction',
            'select',
            [
                'name' => 'featured_auction',
                'label' => __('Featured Status'),
                'options' => ['1' => __('Enabled'), '0' => __('Disabled')],
                'id' => 'buy_it_now',
                'title' => __('Featured Status'),
                'class' => 'required-entry',
                'required' => true,
            ]
        );
         if(!$model->getProductId()) {
             // add dependence javascript block
            $dependenceBlock = $this->getLayout()->createBlock('Magento\Backend\Block\Widget\Form\Element\Dependence');
            $this->setChild('form_after', $dependenceBlock);
            $dependenceBlock->addFieldMap($chooserField->getId(), 'product_id');
        }
        $form->setValues($model->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }
   
  
    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Product Data');
    }
    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Product Data');
    }
    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }
    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }
    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
