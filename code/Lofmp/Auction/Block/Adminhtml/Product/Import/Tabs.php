<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lofmp_Auction
 * 
 * @copyright  Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */
namespace Lofmp\Auction\Block\Adminhtml\Product\Import;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    protected function _construct()
    {
        
        parent::_construct(); 
        $this->setId('product_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Import'));

        $this->addTab(
            'import_product',
            [
            'label' => __('Import Product'),
            'content' => $this->getLayout()->createBlock('Lofmp\Auction\Block\Adminhtml\Product\Import\Tab\Main')->toHtml()
            ]
            );
    }

}
