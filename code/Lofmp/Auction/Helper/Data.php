<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lofmp_Auction
 * @copyright  Copyright (c) 2017 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lofmp\Auction\Helper;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\Pricing\Helper\Data as PriceHelper;
use Lofmp\Auction\Model\ResourceModel\Product\CollectionFactory as AuctCollFactory;
use Lofmp\Auction\Model\AmountFactory;
use Lofmp\Auction\Model\AutoAuction;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_dateTime;

    /**
     * @var TimezoneInterface
     */
    protected $_timezoneInterface;

    /**
     * @var AutoAuctionFactory
     */
    protected $_autoAuction;

    /**
     * @var AuctCollFactory
     */
    protected $_auctionProFactory;



    /**
     * @var AmountFactorye
     */
    protected $_aucAmountFactory;
    /**
     * @var customer
     */
    protected $customer;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
     /**
     * @var PriceCurrencyInterface
     */
    protected $priceFormatter;
     /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * @param \Magento\Framework\App\Helper\Context       $context
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param TimezoneInterface                           $timezoneInterface
     * @param AuctCollFactory                             $auctionProFactory
     * @param AmountFactory                               $aucAmountFactory
     * @param AutoAuctionFactory                          $autoAuction
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        TimezoneInterface $timezoneInterface,
        PriceHelper $priceHelper,
        AuctCollFactory $auctionProFactory,
        AmountFactory $aucAmountFactory,
        AutoAuction $autoAuction,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ProductFactory $productCollectionFactory,
        \Magento\Customer\Model\CustomerFactory $customer,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceFormatter,
        CustomerSession $customerSession
    ) {
        $this->_storeManager = $storeManager;
        $this->_dateTime = $dateTime;
        $this->_auctionProFactory = $auctionProFactory;
        $this->_timezoneInterface = $timezoneInterface;
        $this->priceHelper = $priceHelper;
        $this->_aucAmountFactory = $aucAmountFactory;
        $this->_autoAuction = $autoAuction;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->_customerSession = $customerSession;
        $this->customer = $customer;
        $this->priceFormatter = $priceFormatter;
        parent::__construct($context);
    }
     public function getProductById($product_id) {
        $collection = $this->productCollectionFactory->create()->load($product_id);
      return $collection;
    }
    /**
     * Get current currency code
     *
     * @return string
     */ 
    public function getCurrentCurrencyCode()
    {
      return $this->priceFormatter->getCurrency()->getCurrencyCode();
    }
    
     public function getPriceFomat($price) {
        $currencyCode = $this->getCurrentCurrencyCode();
        return $this->priceFormatter->format(
                    $price,
                    false,
                    null,
                    null,
                    $currencyCode
                );
    }
      /**
      * Get customer id
      * @return customer id
      */
    public function getCustomerById($customer_id) {
        $collection = $this->customer->create()->load($customer_id);
        return $collection;
    }
     /**
     * Return brand config value by key and store
     *
     * @param string $key
     * @param \Magento\Store\Model\Store|int|string $store
     * @return string|null
     */
     public function getConfig($key, $store = null)
    {
       
        $store = $this->_storeManager->getStore($store);
        $result =  $this->scopeConfig->getValue(
            'lofmpauction/'.$key,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store);
        return $result;
    }

     /**
      * Get Configuration Detail of Auction
      * @return array of Auction Configuration Detail
      */
   
    public function getAuctionConfiguration()
    {
        $auctionConfig=[
            'enable' => $this->getConfig('general_settings/enable'),
            'auction_rule' => $this->getConfig('general_settings/auction_rule'),
            'show_bidder' => $this->getConfig('general_settings/show_bidder'),
            'show_price' => $this->getConfig('general_settings/show_price'),
            'reserve_enable' => $this->getConfig('reserve_option/enable'),
            'reserve_price' => $this->getConfig('reserve_option/price'),
            'show_curt_auc_price' => $this->getConfig('general_settings/show_curt_auc_price'),
            'show_auc_detail' => $this->getConfig('general_settings/show_auc_detail'),
            'auto_enable' => $this->getConfig('auto/enable'),
            'auto_auc_limit' => $this->getConfig('auto/limit'),
            'show_auto_details' => $this->getConfig('auto/show_auto_details'),
            'auto_use_increment' => $this->getConfig('auto/use_increment'),
            'show_autobidder_name' => $this->getConfig('auto/show_autobidder_name'),
            'show_auto_bid_amount' => $this->getConfig('auto/show_bid_amount'),
            'show_auto_outbid_msg' => $this->getConfig('auto/show_auto_outbid_msg'),
            'enable_auto_outbid_msg' => $this->getConfig('auto/enable_auto_outbid_msg'),
            'show_winner_msg' => $this->getConfig('general_settings/show_winner_msg'),
            'increment_auc_enable' => $this->getConfig('increment_option/enable'),
            'enable_admin_email' => $this->getConfig('emails/enable_admin_email'),
            'admin_notify_email_template' => $this->getConfig('emails/admin_notify_email_template'),
            'enable_outbid_email' => $this->getConfig('emails/enable_outbid_email'),
            'outbid_notify_email_template' => $this->getConfig('emails/outbid_notify_email_template'),
            'enable_winner_notify_email'  =>  $this->getConfig('emails/enable_winner_notify_email'),
            'winner_notify_email_template' => $this->getConfig('emails/winner_notify_email_template'),
            'admin_email_address' => $this->getConfig('emails/admin_email_address')
        ];
        return $auctionConfig;
    }

    /**
     * @param object $product
     * @return html string
     */
    public function getProductAuctionDetail($product)
    {
        $modEnable = $this->getConfig('general_settings/enable');
        $content = "";
        if ($modEnable) {
            $auctionOpt = $product->getAuctionType();
            $auctionOpt = explode(',', $auctionOpt);

            $auctionData = $this->_auctionProFactory->create()
                                    ->addFieldToFilter('product_id', $product->getEntityId())
                                    ->addFieldToFilter('status', 0)
                                    ->addFieldToFilter('auction_status', 1)
                                    ->getFirstItem()->getData();
            $clock = "";
            $htmlDataAttr = "";
            if ($auctionData) {
                $today = $this->_timezoneInterface->date()->format('m/d/y H:i:s');
                $startAuctionTime = $this->_timezoneInterface->date(new \DateTime($auctionData['start_auction_time']))
                                                ->format('m/d/y H:i:s');
                $stopAuctionTime = $this->_timezoneInterface->date(new \DateTime($auctionData['stop_auction_time']))
                                                ->format('m/d/y H:i:s');
                $difference = strtotime($stopAuctionTime) - strtotime($today);
                if ($difference > 0 && $startAuctionTime < $today) {
                    $clock = '<p class="lofmpcat_count_clock" data-stoptime="'.$auctionData['stop_auction_time']
                                .'" data-diff_timestamp ="'.$difference.'"></p>';
                }
            }

            $auctionData = $this->_auctionProFactory->create()
                                    ->addFieldToFilter('product_id', ['eq'=>$product->getEntityId()])
                                    ->addFieldToFilter('status', ['eq'=>0])->getFirstItem()->getData();
            
            if (isset($auctionData['entity_id'])) {
                $winnerBidDetail = $this->getWinnerBidDetail($auctionData['entity_id']);
                if ($winnerBidDetail && $this->_customerSession->isLoggedIn()) {
                    $winnerCustomerId = $winnerBidDetail->getCustomerId();
                    $currentCustomerId = $this->_customerSession->getCustomerId();
                    if ($currentCustomerId == $winnerCustomerId) {
                        $price = $winnerBidDetail->getBidType() == 'normal' ? $winnerBidDetail->getAuctionAmount():
                                                                $winnerBidDetail->getWinningPrice();
                        $formatedPrice = $this->priceHelper->currency($price, true, false);
                        $htmlDataAttr = 'data-winner="1" data-winning-amt="'.$formatedPrice.'"';
                    }
                }
            }

            /**
             * 2 : use for auction
             * 1 : use for Buy it now
             */
            if (in_array(2, $auctionOpt) && in_array(1, $auctionOpt)) {
                $content = '<div class="auction buy-it-now " '.$htmlDataAttr.'>'.$clock.'</div>';
            } elseif (in_array(2, $auctionOpt)) {
                $content = '<div class="auction" '.$htmlDataAttr.'>'.$clock.'</div>';
            } elseif (in_array(1, $auctionOpt)) {
                $content = '<div class="buy-it-now"></div>';
            }
        }
        return $content;
    }

    /**
     * $auctionId
     * @param int $auctionId auction product id
     * @return AmountFactory || AutoAuctionFactory
     */
    public function getWinnerBidDetail($auctionId)
    {
        $aucAmtData = $this->_aucAmountFactory->create()->getCollection()
                                        ->addFieldToFilter('auction_id', ['eq'=>$auctionId])
                                        ->addFieldToFilter('winning_status', ['eq'=>1])
                                        ->addFieldToFilter('status', ['eq'=>0])->getFirstItem();
        
        if ($aucAmtData->getEntityId()) {
            $aucAmtData->setBidType('normal');
            return $aucAmtData;
        } else {
            $aucAmtData = $this->_autoAuction->getCollection()
                                        ->addFieldToFilter('auction_id', ['eq'=> $auctionId])
                                        ->addFieldToFilter('flag', ['eq'=>1])
                                        ->addFieldToFilter('status', ['eq'=>0])->getFirstItem();
            if ($aucAmtData->getEntityId()) {
                $aucAmtData->setBidType('auto');
                return $aucAmtData;
            }
        }
        return false;
    }

    /**
     * get incremental price value
     * @var float $minAmount
     * @var float
     */
    public function getIncrementPriceAsRange($minAmount)
    {
        $incPriceRang = $this->getConfig('increment_option/price_range');
        
        $incPriceRang = json_decode($incPriceRang, true);
        if($incPriceRang) {
            foreach ($incPriceRang as $range => $value) {
                $range = explode('-', $range);
                if ($minAmount >= $range[0] && $minAmount <= $range[1]) {
                    return floatval($value);
                }
            }
        }
        return false;
    }

    /**
     * get Active Auction Id
     * @param $productId int
     * @return int|false
     */
    public function getActiveAuctionId($productId)
    {
        $auctionData = $this->_auctionProFactory->create()
                                        ->addFieldToFilter('product_id', ['eq'=>$productId])
                                        ->addFieldToFilter('status', ['eq'=>0])
                                        ->setOrder('entity_id')->getFirstItem();

        return $auctionData->getEntityId() ? $auctionData->getEntityId() : false;
    }
}
