<?php
/**
 * Webkul Software.
 *
 * @category   Webkul
 * @package    Webkul_Agorae
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2017 Webkul Software Private Limited (
 https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Seasia\Custom\Helper;


/**
 * Webkul Agorae Helper Data
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $_objectManager;
    protected $_countryCollectionFactory;

    public function __construct(\Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory,\Magento\Framework\App\Helper\Context $context,array $data = []) 
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();     
        parent::__construct($context);
        $this->_countryCollectionFactory = $countryCollectionFactory; 
    }

    public function getCountryCollection()
    {
        $collection = $this->_countryCollectionFactory->create()->loadByStore();
        return $collection;
    }

    public function getBaseUrl() {
        $storeManager = $this->_objectManager->get('\Magento\Store\Model\StoreManagerInterface');

        return $storeManager->getStore()->getBaseUrl();
    }

    protected function getTopDestinations()
    {
        $destinations = (string)$this->_scopeConfig->getValue(
            'general/country/destinations',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        return !empty($destinations) ? explode(',', $destinations) : [];
    }

    /**
     * Retrieve list of countries in array option
     *
     * @return array
     */
    public function getCountries()
    {
        return $options = $this->getCountryCollection()
                ->setForegroundCountries($this->getTopDestinations())
                    ->toOptionArray();
    }

    public function getCountryName($code)  {
        return $this->_objectManager->create('\Magento\Directory\Model\Country')->load($code)->getName();
    }
}
      